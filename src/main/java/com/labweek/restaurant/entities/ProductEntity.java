package com.labweek.restaurant.entities;

import com.labweek.restaurant.dtos.ProductDTO;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Entity(name="restaurant")
@Table(name="restaurant")
@NoArgsConstructor
@EqualsAndHashCode(of="id")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    @Column(unique = true)
    private String name;
    private String description;
    private double price;
    private int amount;


    public ProductEntity(ProductDTO restaurantDTO){
        this.name = restaurantDTO.name();
        this.description = restaurantDTO.description();
        this.price = restaurantDTO.price();
        this.amount = restaurantDTO.amount();
    }
}
