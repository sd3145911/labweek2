package com.labweek.restaurant.services;

import com.labweek.restaurant.dtos.ProductDTO;
import com.labweek.restaurant.entities.ProductEntity;
import com.labweek.restaurant.repositories.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public ProductEntity createProduct(ProductDTO dto){
        var product = new ProductEntity(dto);
        return this.repository.save(product);
    }

    public List<ProductEntity> getAll(){
        return repository.findAll();
    }

    public void deleteProduct(UUID id){
        repository.deleteById(id);
    }

    public ProductEntity updateProduct(UUID id, ProductDTO productUpdated){
        ProductEntity product = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product not found with id: " + id));

        if(productUpdated.name() != null) product.setName(productUpdated.name());
        if(productUpdated.description() != null) product.setDescription(productUpdated.description());
        if(productUpdated.price() > 0) product.setPrice(productUpdated.price());
        if(productUpdated.amount() > 0) product.setAmount(productUpdated.amount());

        return repository.save(product);
    }

    public ProductEntity getProductById(UUID id){
        ProductEntity product = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));

        return product;
    }

    public ProductEntity getProductByName(String name){
        ProductEntity product = repository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));

        return product;
    }

    public List<ProductEntity> getProductByPrice(double price){
        return repository.findAllByPrice(price)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public List<ProductEntity> getProductByAmount(int amount){
        return repository.findAllByAmount(amount)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

}
