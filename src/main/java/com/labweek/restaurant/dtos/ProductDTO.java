package com.labweek.restaurant.dtos;

public record ProductDTO(String name, String description, double price, int amount){

}
