package com.labweek.restaurant.repositories;

import com.labweek.restaurant.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, UUID> {
    Optional<ProductEntity> findByName(String name);
    Optional<List<ProductEntity>> findAllByPrice(double name);
    Optional<List<ProductEntity>> findAllByAmount(int amount);


}
