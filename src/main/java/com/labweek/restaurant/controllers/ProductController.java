package com.labweek.restaurant.controllers;

import com.labweek.restaurant.dtos.ProductDTO;
import com.labweek.restaurant.entities.ProductEntity;
import com.labweek.restaurant.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<ProductEntity> createProduct(@RequestBody ProductDTO dto){
        var newProduct = productService.createProduct(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(newProduct);
    }

    @GetMapping
    public ResponseEntity<List<ProductEntity>> getAllProducts(){
        return ResponseEntity.ok(productService.getAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ProductEntity> deleteProduct(@PathVariable UUID id){
        productService.deleteProduct(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ProductEntity> updateProduct(@PathVariable UUID id, @RequestBody ProductDTO productDTO){
        var product = productService.updateProduct(id, productDTO);

        return ResponseEntity.status(HttpStatus.OK).body(product);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<ProductEntity> getProductById(@PathVariable UUID id){
        return ResponseEntity.ok(productService.getProductById(id));
    }

    @GetMapping("/name")
    public ResponseEntity<ProductEntity> getProductById(@RequestParam String name){
        return ResponseEntity.ok(productService.getProductByName(name));
    }

    @GetMapping("/price")
    public ResponseEntity<List<ProductEntity>> getProductByPrice(@RequestParam double price){
        return ResponseEntity.ok(productService.getProductByPrice(price));
    }

    @GetMapping("/amount")
    public ResponseEntity<List<ProductEntity>> getProductByAmount(@RequestParam int amount){
        return ResponseEntity.ok(productService.getProductByAmount(amount));
    }
}
